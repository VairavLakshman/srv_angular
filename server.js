var fs = require('fs'),
    express = require('express'),
    routes = require('./routes'),
    path = require('path'),
    config = require('./config/config.js'),
    passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth2').Strategy,
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    logger = require("morgan"),
    session = require("express-session"),
    engines = require('ejs'),
    GoogleSpreadsheet = require('google-spreadsheet'),
    async = require('async'),
    http = require('http'),
    pg = require('pg'),
    doc = new GoogleSpreadsheet(config.google.doc),
    connectionString = config.google.connectionString,
    client = new pg.client(connectionString),
    startDate,
    endDate,
    projectName,
    query,
    mailId,
    sheet,
    userName;
pg.defaults.ssl = true;
client.connect();

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

passport.use(new GoogleStrategy({
  clientID: config.google.clientId,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL,
  passReqToCallback: config.google.passReqToCallback
},
function (request, accessToken, refreshToken, profile, done) {
  process.nextTick(function () {
    return done(null, profile);
  });
}));

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/public/views');
app.engine('html', engines.renderFile);
app.set('view engine', 'html');
app.use(logger('dev'));
app.use(cookieParser());
app.use(session({ secret: 'my_precious' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());
app.use(passport.initialize());
app.use(passport.session({ resave: true, saveUninitialized: true, secret: "mypassword"}));
app.use(express.static(__dirname + '/public'));

app.get('/', routes.index);

app.get('/auth/google', passport.authenticate('google', { scope: [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'
]}));

app.get('/auth/google/callback', passport.authenticate('google', {failureRedirect: '/'}),
  function (req, res) {
    res.redirect('/main');
  });

app.get('/notes', function (req, res) {
  res.render("notes.html");
});

app.get('/logout', function (req, res) {
  req.logout();
  res.redirect('/');
});

app.get('/main', function (req, res) {
  if (req.isAuthenticated()) {
    if (req.user.name.givenName === '') {
      req.user.name.givenName = 'user';
    }
    if (config.google.updateDBRhythmData) {
      async.series([
        function setAuth(step) {
          var credits = require('./config/gsheet_credentials.json');
          doc.useServiceAccountAuth(credits, step);
        }
      ])
    }
  }
})
